# -*- coding: utf-8 -*-
"""
Created on Tue Jun 22 14:00:35 2021

@author: kzy
"""

import numpy as np
import matplotlib.pyplot as plt
from DendriteUndercooling import KGTmat
from ThreeDThesis_Call import Run
from Al10Mn2Ce import Al10Mn2Ce_Call
from Al20Mn2Ce import Al20Mn2Ce_Call
import glob
import pandas as pd

Vmin = -1.5; Vmax = 0
Run(Vmin = Vmin, Vmax = Vmax)

Al10Mn2Ce = Al10Mn2Ce_Call()
Al20Mn2Ce = Al20Mn2Ce_Call()

Simulations = glob.glob('Data/*/')

Al10_store = []; Al20_store = []
for Simulation in Simulations:
    
    File = glob.glob(Simulation + '*.csv')
    
    
    Data = pd.read_csv(File[0])

    Width = 2*np.max(Data['y'])*1000
    Height = -2*np.min(Data['z'])*1000
    
    
    print(Width, Height)
    
    Al10_Count = 0
    Al20_Count = 0
        
    for itr, V in enumerate(Data['V']):
        
        _, uc_Al10, _, _, _, _, _ = Al10Mn2Ce.Calculate(Data['G'][itr],V)
        _, uc_Al20, _, _, _, _, _ = Al20Mn2Ce.Calculate(Data['G'][itr],V)
        
        
        if np.isnan(uc_Al10):
            uc_Al10 = 0
        if np.isnan(uc_Al20):
            uc_Al20 = 0
            
            
        if ((Al10Mn2Ce.Al10Mn2Ce.Tliq - uc_Al10)  > (Al20Mn2Ce.Al20Mn2Ce.Tliq -uc_Al20)):        
            Al10_Count += 1         
        else:
            Al20_Count += 1
                
    Al10_store.append(Al10_Count/len(Data['V']))
    Al20_store.append(Al20_Count/len(Data['V']))
    
plt.figure()
V = np.logspace(Vmin,Vmax,50)*1000
plt.plot(V,Al10_store, label = 'Al10_Fraction')
plt.plot(V,Al20_store, label = 'Al20_Fraction')

plt.xlabel('Laser Velocity')
plt.ylabel('Fraction of Phase')
plt.xscale('log')
plt.legend()
