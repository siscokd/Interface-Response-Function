# -*- coding: utf-8 -*-
"""
Created on Thu Mar 18 14:42:28 2021

@author: Kevin
"""

import os 
import shutil

class ParameterGeneration: 
    def __init__(self, name, pathfile):
        
        self.SetDefaultParams()
        
        self.InputsFolder = 'TestInputs'
        try:
            os.mkdir(self.InputsFolder)
        except:
            print('Folder Already Exists')
            
        self.name = name
        self.Pathfile = pathfile
        self.Outputfile = os.getcwd() +"\\" +  self.InputsFolder + '\\Path.txt'
        
    
    def SetDefaultParams(self):
        
        ## Beam
        
        self.Width_X = 10.0e-6 # m
        self.Width_Y = 10.0e-6 # m
        self.Depth_Z = 1.0e-6 # m
        
        self.Power = 1200 # Watts
        self.Efficiency = 1.0 
        

        ## Domain
        
        self.X_Min = -0.001
        self.X_Max = 0.006 #m
        self.X_Res = 25e-6 #
        
        self.Y_Min = -0.001
        self.Y_Max = 0.002 #m
        self.Y_Res = 25e-6 #
        
        self.Z_Min = -0.001
        self.Z_Max = 0.000 #m
        self.Z_Res = 25e-6 #
        
        
        
        ## Material
        
        self.T_0 = 1273.0
        self.T_L = 1610.0
        self.k = 26.6
        self.c = 600.0
        self.p = 7451.0
        
        ## Settings
        
        self.Timestep = 1e-4
        self.Mode = 3
        self.MaxThreads = 8
        self.Output_Mode = 1
    
    def Write(self):
        
        self.WriteBeam()
        self.WriteDomain()
        self.WriteMaterial()
        self.WriteSettings()
        self.WriteParamInput()
        #self.CopyPathFile()
        
        return (' ' + os.getcwd() + '\\' + self.InputsFolder + '\\ParamInput.txt')
        
        
    def WriteBeam(self):
        
        f = open((os.getcwd() + "\\" + self.InputsFolder + "\\Beam.txt"),'w')
        try:
            f.write("Shape\n{\n\tWidth_X\t" + str(self.Width_X) + '\n\tWidth_Y\t' + 
                    str(self.Width_Y) + '\n\tDepth_Z\t' + str(self.Depth_Z) +'\n}')
            f.write('\nIntensity\n{\n\tPower\t' + str(self.Power) + '\n\tEfficiency\t'
                    + str(self.Efficiency) + '\n}')
        finally:
            if f is not None:
                f.close()

    
    def WriteDomain(self):
        
        f = open((os.getcwd()  +"\\" + self.InputsFolder +  "\\Domain.txt"),'w')
        try:
            ## X
            f.write('X\n{\n\tMin\t' +  str(self.X_Min) + '\n\tMax\t' + str(self.X_Max)
                    + '\n\tRes\t' + str(self.X_Res) + '\n}\n')
            ## Y
            f.write('Y\n{\n\tMin\t' +  str(self.Y_Min) + '\n\tMax\t' + str(self.Y_Max)
                    + '\n\tRes\t' + str(self.Y_Res) + '\n}\n')
            ## Z
            f.write('Z\n{\n\tMin\t' +  str(self.Z_Min) + '\n\tMax\t' + str(self.Z_Max)
                    + '\n\tRes\t' + str(self.Z_Res) + '\n}\n')
        finally:
            if f is not None:
                f.close()

    def WriteMaterial(self):
        
        
        f = open((os.getcwd() + "\\" + self.InputsFolder + "\\Material.txt"),'w')
        try:
            ## Constants
            f.write('Constants\n{\n\tT_0\t'+ str(self.T_0) + '\n\tT_L\t' + str(self.T_L) +
                    '\n\tk\t' + str(self.k) + '\n\tc\t' + str(self.c) +'\n\tp\t' +str(self.p) + '\n}')
        finally:
            if f is not None:
                f.close()
                
    def WriteSettings(self):
        

        f = open((os.getcwd() + "\\" + self.InputsFolder + "\\Settings.txt"),'w')
        try:
            ## Constants
            f.write('Simulation\n{\n\tTimeStep\t' + str(self.Timestep) + '\n\tMode\t'+
            str(self.Mode) + '\n\tMaxThreads\t' + str(self.MaxThreads) + '\n}\nOutput\n{\n\tMode\t' 
            + str(self.Output_Mode) + '\n}')
        finally:
            if f is not None:
                f.close()
                
    def WriteParamInput(self):
        
        
        f = open((os.getcwd() + "\\" + self.InputsFolder + "\\ParamInput.txt"),'w')
        try:
            ## Constants
            FirstChain = '\n\tMaterial\tTestInputs/Material.txt\n\tBeam\tTestInputs/Beam.txt\n\tPath\tTestInputs/Path.txt\n}'
            SecondChain = '\nOptions\n{\n\tDomain\tTestInputs/Domain.txt\n\tSettings\tTestInputs/Settings.txt\n}'
            f.write('Simulation\n{\n\tName\t' + self.name + FirstChain + SecondChain)
        finally:
            if f is not None:
                f.close()
                
                
    def CopyPathFile(self):
        print(self.Pathfile)
        print(self.Outputfile)
        shutil.copyfile(self.Pathfile,self.Outputfile)
        
    def UpdateMaterialParameters(self,Preheat_Temperature,Liquidus_Temperature,ThermalConductivity,SpecificHeat,Density):
        
        self.T_0 = Preheat_Temperature
        self.T_L = Liquidus_Temperature
        self.k = ThermalConductivity
        self.c = SpecificHeat
        self.p = Density
        
    def UpdateBeamParameters(self,Power,Efficiency,Width_X,Width_Y,Depth_Z):
              
        self.Width_X = Width_X # m
        self.Width_Y = Width_Y # m
        self.Depth_Z = Depth_Z # m
        
        self.Power = Power # Watts
        self.Efficiency = Efficiency
        
        
    def UpdateSettings(self, Timestep, Mode, MaxThreads,Output_Mode):
        
        self.Timestep = Timestep
        self.Mode = Mode
        self.MaxThreads = MaxThreads
        self.Output_Mode = Output_Mode
    
    def UpdateDomain(self,X_Values,Y_Values,Z_Values):
        
        self.X_Min = X_Values[0]
        self.X_Max = X_Values[1]
        self.X_Res = X_Values[2]
        
        self.Y_Min = Y_Values[0]
        self.Y_Max = Y_Values[1]
        self.Y_Res = Y_Values[2]
        
        self.Z_Min = Z_Values[0]
        self.Z_Max = Z_Values[1]
        self.Z_Res = Z_Values[2]
        
        
    def WritePathFile(self, Velocity):
        
        
        f = open((os.getcwd() + "\\TestInputs\\Path.txt"),'w')
        try:
            FirstRow = 'Mode	X(mm)	Y(mm)	Z(mm)	Pmod	Time(s)'
            SecondRow = '1	0	0	0	0	1.00E-04'
            ThirdRow = '0	5	0	0	1	' + str(Velocity)
    
    
            f.write(FirstRow +'\n' + SecondRow + '\n' + ThirdRow)
        finally:
            if f is not None:
                f.close()
    

