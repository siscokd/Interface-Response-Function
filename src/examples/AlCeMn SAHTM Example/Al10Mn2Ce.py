# -*- coding: utf-8 -*-
"""
Created on Wed Jul 21 16:46:24 2021

@author: kzy
"""
from DendriteUndercooling import KGTmat

class Al10Mn2Ce_Call:

    
    def __init__(self, **kwargs):
        Dl = 5e-9
        a0 = 5e-9
        
        self.Al10Mn2Ce = KGTmat(
            Tf=1219.1,    # Melting temperature of Alloy, K
            Tliq=1219.1,    # Liquidus temperature, K
            gam=0,     # Gibbs-Thomson coefficient
            Dl=Dl,        # Mass diffusivity in liquid, m2/s
            V0=4000,        # Speed of sound, m/s
            a0=a0,        # Length scale for solute trapping, m
            rho=4080,     # Density of Al10Mn2ce, kg/m3
            Lf=28238,      # Latent heat of fusion,  From Yings Paper
            alpha=9e-4,     # Thermal diffusivity
            cl=700,         # Liquid specific heat
            )
        
        
        
        
        Al10Mn2Ce_Ce_K = .1/.26955
        Al10Mn2Ce_Mn_K = .08/.21137
        
        
        self.Al10Mn2Ce.gam = self.Gibbs_Thomson_Coeff(Interfacial_Energy = .321, MeltTemp = self.Al10Mn2Ce.Tf, EnthalpyOfFusion = 28238/(9.828e-6)) #  From Yings Paper
        self.Al10Mn2Ce.add_element(name='Ce', C0=0.1, k0=Al10Mn2Ce_Ce_K, m0=-(1/0.000492))
        self.Al10Mn2Ce.add_element(name='Mn', C0=0.08, k0=Al10Mn2Ce_Mn_K, m0=-(1/0.000381))
    
    
    
    def Calculate(self,G,V):
        R, uc, constitutional, curvature, kinetics, cell, thermal = self.Al10Mn2Ce.dendriteTip(G, V)
        return R, uc, constitutional, curvature, kinetics, cell, thermal


    def Gibbs_Thomson_Coeff(self,Interfacial_Energy, MeltTemp, EnthalpyOfFusion):
    
        Interfacial_Energy = Interfacial_Energy
        Tm = MeltTemp # Melt Temp Kelvin
        Enthalpy_of_Fusion_PerVol = EnthalpyOfFusion
        Gibbs_Thomson_Ceff = (Interfacial_Energy*(Tm))/(Enthalpy_of_Fusion_PerVol)
        
        
        return Gibbs_Thomson_Ceff

