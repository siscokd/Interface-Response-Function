import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy import interpolate


def Iv3d(u):
    gamma = 0.5772156649015328606
    a = np.exp(-gamma)
    b = np.sqrt((2 * (1 - a)) / (a * (2 - a)))
    hinf = (1 - a) * (a ** 2 - 6 * a + 12) / (3 * a * ((2 - a) ** 2) * b)
    q = (20 / 47) * (u ** (np.sqrt(31 / 26)))
    h = (1 / (1 + u * np.sqrt(u))) + (hinf * q / (1 + q))
    ytop = (np.exp(-u) * np.log(1 + (a / u) - ((1 - a) / ((h + b * u) ** 2))))
    ybtm = a + (1 - a) * np.exp(-u / (1 - a))
    y = u * np.exp(u) * ytop / ybtm
    if ytop < 1e-10:
        y = 0
    return y



class KGTmat:
    def __init__(self, **kwargs):
        self.Tf = kwargs.get('Tf')
        self.Tliq = kwargs.get('Tliq', None)
        self.Teut = kwargs.get('Teut')
        self.Ceut = kwargs.get('Ceut')
        self.gam = kwargs.get('gam')
        self.Dl = kwargs.get('Dl', 1e-9)
        self.sigst = kwargs.get('sigst', 0.025)
        self.V0 = kwargs.get('V0')
        self.a0 = kwargs.get('a0')
        self.rho = kwargs.get('rho')
        self.Lf = kwargs.get('Lf')
        self.alpha = kwargs.get('alpha')
        self.cl = kwargs.get('cl')

        self.muk = None
        self.elements = []
        self.element_names = []



    class element:
        def __init__(self, C0, k0, m0):
            self.C0 = C0
            self.k0 = k0
            self.m0 = m0
            self.kv = k0
            self.mv = m0
            self.xic = None
            self.muk = None

    def add_element(self, **kwargs):
        name = kwargs.get('name')
        C0 = kwargs.get('C0')
        k0 = kwargs.get('k0')
        m0 = kwargs.get('m0')
        self.elements.append(self.element(C0, k0, m0))
        self.element_names.append(name)
        self.elements[-1].muk = self.V0 * (1 - self.elements[-1].k0) / self.elements[-1].m0

    def vparams(self, V):
        temp = self.a0 * V / self.Dl
        for el in self.elements:
            el.kv = (el.k0 + temp) / (1 + temp)
            el.mv = el.m0 * (1 + ((el.k0 - el.kv + el.kv * np.log(el.kv / el.k0)) / (1 - el.k0)))

    def calc_Tliq(self):
        self.Tliq = self.Tf
        for el in self.elements:
            self.Tliq += el.m0 * el.C0

    def dendriteTip(self, G, V):
        R = 1e-8
        not_conv = True
        tol = 1e-5
        relax = 0.9

        self.vparams(V)

        # Converge tip radius
        while(not_conv):
            Pe = R * V / (2 * self.Dl)
            denom = 0
            for el in self.elements:
                Ct = el.C0 / (1 - (1 - el.kv)*Iv3d(Pe))
                el.xic = np.sqrt(1 + (1 / (self.sigst * Pe**2))) - 1 + 2*el.kv
                el.xic = 1 - (2 * el.kv / el.xic)
                Gc = -(V / self.Dl) * Ct * (1 - el.kv)
                denom += (-el.mv * Gc * el.xic)

            denom = self.sigst * (-denom - G)
            R_temp = R
            
            try:
                R = np.sqrt(self.gam / denom)
                R = R_temp + relax * (R - R_temp)
                dif = np.abs(R - R_temp)
                if dif/R < tol or np.isnan(R):
                    not_conv = False
            except:
                continue

        # Loop through alloying elements
        constitutional = 0
        kinetics = 0
        for el in self.elements:
            # Constitutional supercooling
            dTv = el.mv * el.C0 * (el.kv - 1) / el.kv
            constitutional += (el.kv * dTv * Iv3d(Pe)) / (1 - (1 - el.kv) * Iv3d(Pe))
            constitutional = constitutional + (el.m0 - el.mv) * el.C0

            # Kinetic undercooling
            kinetics += V / el.muk

        # Thermal undercooling
        Peclet_thermal = V * R / (2 * self.alpha)
        thermal = self.Lf * Iv3d(Peclet_thermal) / self.cl

        # Curvature undercooling
        curvature = (2 * self.gam / R)

        # Cellular undercooling
        cell = G * self.Dl / V

        # Total undercooling
        total = constitutional + curvature + kinetics + cell + thermal
        return R, total, constitutional, curvature, kinetics, cell, thermal





