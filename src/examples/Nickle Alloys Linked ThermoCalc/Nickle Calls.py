# -*- coding: utf-8 -*-
"""
Created on Mon Jul 26 17:13:19 2021

@author: ksisco
"""

import numpy as np
import matplotlib.pyplot as plt
from tc_python import *
from PlanarGrowthFunction import PlanarGrowth
from DendriteUndercooling_WithThermoCalc import KGTmat

G = 1e6; Vs = np.logspace(-5,2,100)
fig1, ax1 = plt.subplots()



def IRF_Calc(ThermoCalc, Composition, main_phase, Lf = 0):
    
    IRF = KGTmat(Dl= 5e-9, V0=5600, a0=5e-9, ThermalConductivity = 11.2, cl = 435, Lf = Lf)
    Tliq, Ts, Element = IRF.generate_coefficients_Thermocalc(ThermoCalc_Interface, composition, main_phase = main_phase, database = 'TCNI9', balance = 'Ni')
    IRF.alpha = IRF.ThermalConductivity/(IRF.rho*IRF.cl)

    Tdc = [];
    Temperatures = []
    IRF.gam = 1e-7
    for V in Vs:
        R, total, constitutional, curvature, kinetics, cell, thermal = IRF.dendriteTip(G, V)
        Tdc.append(total)
        Temperatures.append([R, total, constitutional, curvature, kinetics, cell, thermal])
    return Tliq, Tdc, Element, Temperatures
    

with TCPython() as ThermoCalc_Interface:

    composition = {'Fe': 0.093*100, 'Cr': 0.19*100, 'Nb': .042*100, 'Mo': 0.03*100, 'Ti': 0.095*100, 'Ni':-1}   #wt%

    Phases = ['FCC_A1']
    for Phase in Phases:
    
        Tliq, Tdc, element, Temperatures = IRF_Calc(ThermoCalc_Interface, composition, main_phase = Phase)
        ax1.plot(Vs,Tliq - np.array(Tdc), label = Phase)
        
        if Phase == 'FCC_A1':
            PG = PlanarGrowth(element)
            Tp = PG.PlanarGrowthUndercoolingCalculation(Ts = 1182.5, a0 = 5e-9, Vs = Vs, Di = 5e-9, mu=10)
            ax1.plot(Vs,Tp, label = Phase + ' Planar')
        
    #ax1.plot(Vs,TP)
    ax1.set_xscale('log')
    ax1.set_ylabel('T (K)')
    ax1.set_xlabel('Solidification Velocity (m/s)')
    ax1.set_xlim([1e-5,100])
    ax1.legend()
