# -*- coding: utf-8 -*-
"""
Created on Wed Oct 28 11:57:46 2020

@author: Kevin
"""

import numpy as np

class PlanarGrowth:
    
    def __init__(self, elements):
        self.elements = elements
    
        
    def Calc_Kv(self, k0,a0,Vs,Di):
    
        Kv = (k0 + a0*(Vs/Di))/(1+a0*(Vs/Di))
    
        return Kv
    
    def Calc_Mv(self, M0,Kv,K0):
    
        Mv = M0*((1-Kv*(1-np.log(Kv/K0)))/(1-K0))
    
        return Mv
           
    def PlanarGrowthUndercoolingCalculation(self, Ts,a0,Vs,Di,mu):
        
        Kv_store = {}; Mv_store = {}
        T_planar = 0
        for itr,ele in enumerate(self.elements):
                        
            Kv = self.Calc_Kv(ele.k0,a0,Vs,Di)
            Mv = self.Calc_Mv(ele.m0,Kv,ele.k0)
            
            T_planar += ele.c0*((Mv/Kv)-(ele.m0/ele.k0))
            

            
        
        T_planar += Ts
        T_planar -= Vs/mu
    
        return T_planar


