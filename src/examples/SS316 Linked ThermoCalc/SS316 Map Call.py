# -*- coding: utf-8 -*-
"""
Created on Mon Jul 26 17:13:19 2021

@author: ksisco
"""
D_BCC = 5e-9; D_FCC = 5e-9


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy import interpolate
from tc_python import *
from scipy.interpolate import interp1d
from PlanarGrowthFunction import PlanarGrowth
from DendriteUndercooling_WithThermoCalc import KGTmat


with TCPython() as ThermoCalc_Interface:
    fig1, ax1 = plt.subplots()
    G = 1e4; Vs = np.logspace(-7,1,100)
    composition = {'Cr': 16.81, 'Ni': 11.92, 'Mo': 2.43, 'Fe':-1}   
    
    
    SS316L_FCC = KGTmat(Dl=D_FCC,V0=5100, a0=5e-9, ThermalConductivity = 15)
    FCC_Tliq, FCC_Ts, FCC_Element = SS316L_FCC.generate_coefficients_Thermocalc(ThermoCalc_Interface, composition, main_phase = 'FCC_A1', database = 'TCFE9', balance = 'Fe')
    SS316L_FCC.gam = 2.6e-7
    
    
    FCC_PG = PlanarGrowth(FCC_Element)

    T_FCC = []; Rs = []; TP_FCC = []
    for V in Vs:
        R, total, constitutional, curvature, kinetics, cell, thermal = SS316L_FCC.dendriteTip(G, V)
        T_FCC.append(total)
        Rs.append(R)
        
        
        T_planar = FCC_PG.PlanarGrowthUndercoolingCalculation(Ts = FCC_Ts-.5,a0 = D_FCC,Vs = V ,Di = D_FCC ,mu = 10)
        TP_FCC.append(T_planar)

        

    ax1.plot(Vs, FCC_Tliq -np.array(T_FCC), label = 'fcc', color = 'red')
    ax1.plot(Vs, TP_FCC, label = 'FCC Planar', color = 'red', linestyle = 'dashed')
    ax1.set_xscale('log')
    
    composition['Fe'] = -1
    SS316L_BCC = KGTmat(Dl=D_BCC,V0=5100,a0=5e-9, ThermalConductivity = 15) 
    BCC_Tliq, BCC_Ts, BCC_Element = SS316L_BCC.generate_coefficients_Thermocalc(ThermoCalc_Interface, composition, main_phase = 'bcc_a2', database = 'TCFE9', balance = 'Fe')
    SS316L_BCC.gam = 3.2e-7

    BCC_PG = PlanarGrowth(BCC_Element)

    T_BCC = []; Rs = []; TP_BCC = []
    for V in Vs:
        R, total, constitutional, curvature, kinetics, cell, thermal = SS316L_BCC.dendriteTip(G, V)
        T_BCC.append(total)
        Rs.append(R)
        
        T_planar = BCC_PG.PlanarGrowthUndercoolingCalculation(Ts = 1706.2,a0 = 5e-9,Vs = V ,Di = D_BCC,mu = 10)
        TP_BCC.append(T_planar)
        
        
        

    ax1.plot(Vs, BCC_Tliq -np.array(T_BCC), label = 'bcc', color = 'black')
    ax1.plot(Vs, TP_BCC, label = 'BCC Planar',color = 'black', linestyle = 'dashed')
    
    ax1.legend()
    ax1.set_xscale('log')
    ax1.set_xlim([1e-6,10])
    ax1.set_xlabel('Solidification Velocity (m/s)')
    ax1.set_ylabel('Dendrite Tip Temperature (K)')


    fig1.savefig('IRF' + str(D_BCC) + '.png', dpi = 300)

    Gradients = np.logspace(4,7,75)
    Velocities = np.logspace(-6,2,75)
    
    fig2,ax2 = plt.subplots()
    for Grad in Gradients:
        for Vel in Velocities:
            
             R, total_FCC, constitutional, curvature, kinetics, cell, thermal = SS316L_FCC.dendriteTip(Grad, Vel)
             R, total_BCC, constitutional, curvature, kinetics, cell, thermal = SS316L_BCC.dendriteTip(Grad, Vel)
             T_planar_BCC = BCC_PG.PlanarGrowthUndercoolingCalculation(Ts = 1708,a0 = 5e-9,Vs = Vel ,Di = D_BCC,mu = 10)
             T_planar_FCC = FCC_PG.PlanarGrowthUndercoolingCalculation(Ts =  FCC_Ts,a0 = 5e-9,Vs = Vel ,Di = D_FCC,mu = 10)
             
             FCC_Temp = FCC_Tliq - total_FCC
             BCC_Temp = BCC_Tliq - total_BCC
             

             if np.isnan(FCC_Temp):
                 FCC_Temp = 0
                 
             if np.isnan(BCC_Temp):
                 BCC_Temp = 0    
                   
            
             if FCC_Temp > BCC_Temp and FCC_Temp > T_planar_BCC  and FCC_Temp > T_planar_FCC:
                 ax2.scatter(Vel,Grad,color = 'red', marker = 's')
             elif BCC_Temp > FCC_Temp and BCC_Temp > T_planar_BCC  and BCC_Temp > T_planar_FCC:
                 ax2.scatter(Vel,Grad,color = 'black', marker = 's')
             elif T_planar_BCC > BCC_Temp and T_planar_BCC > FCC_Temp  and  T_planar_BCC > T_planar_FCC:
                 ax2.scatter(Vel,Grad,color = 'darkgray', marker = 's')
             elif T_planar_FCC > BCC_Temp and T_planar_FCC > FCC_Temp  and  T_planar_FCC > T_planar_BCC:
                 ax2.scatter(Vel,Grad,color = 'deepskyblue', marker = 's')
                 
                 
    ax2.set_xlim([1e-6,10])
    ax2.set_ylim([1e4,1e7])
    ax2.set_xlabel('Solidification Velocity (m/s)')
    ax2.set_ylabel('Thermal Gradient (K/m)')
    ax2.set_xscale('log')
    ax2.set_yscale('log')
    fig2.savefig('IRF Map' + str(D_BCC) + '.png', dpi = 300)
