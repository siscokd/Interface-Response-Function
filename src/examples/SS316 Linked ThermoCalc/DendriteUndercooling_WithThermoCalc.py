# -*- coding: utf-8 -*-
"""
Created on Tue Jul 13 14:17:45 2021

@author: kzy
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy import interpolate
from tc_python import *
from scipy.interpolate import interp1d
from PlanarGrowthFunction import PlanarGrowth

def Iv3d(u):
    gamma = 0.5772156649015328606
    a = np.exp(-gamma)
    b = np.sqrt((2 * (1 - a)) / (a * (2 - a)))
    hinf = (1 - a) * (a ** 2 - 6 * a + 12) / (3 * a * ((2 - a) ** 2) * b)
    q = (20 / 47) * (u ** (np.sqrt(31 / 26)))
    h = (1 / (1 + u * np.sqrt(u))) + (hinf * q / (1 + q))
    ytop = (np.exp(-u) * np.log(1 + (a / u) - ((1 - a) / ((h + b * u) ** 2))))
    ybtm = a + (1 - a) * np.exp(-u / (1 - a))
    y = u * np.exp(u) * ytop / ybtm
    if ytop < 1e-10:
        y = 0
    return y

class KGTmat:
    def __init__(self, **kwargs):
        self.Tf = kwargs.get('Tf', None) # Not Sure
        self.Tliq = kwargs.get('Tliq', None) #Liquidus Temperature
        self.gam = kwargs.get('gam', None) # gibbs free energy
        self.Dl = kwargs.get('Dl') # Diffusivity in liquid
        self.sigst = kwargs.get('sigst', 0.025)
        self.V0 = kwargs.get('V0') # Speed of sound in material
        self.a0 = kwargs.get('a0') # characterisitc diffusion length
        self.rho = kwargs.get('rho', None) # density
        self.Lf = kwargs.get('Lf', None)
        self.alpha = kwargs.get('alpha', None)
        self.cl = kwargs.get('cl', None) # specific Heat???
        self.ThermalConductivity = kwargs.get('ThermalConductivity', None)
        self.muk = None
        self.elements = []
        self.element_names = []



    class element:
        def __init__(self, c0, k0, m0):
            self.c0 = c0
            self.k0 = k0
            self.m0 = m0
            self.kv = k0
            self.mv = m0
            self.xic = None
            self.muk = None

    def add_element(self, **kwargs):
        name = kwargs.get('name')
        c0 = kwargs.get('c0')
        k0 = kwargs.get('k0')
        m0 = kwargs.get('m0')
        self.elements.append(self.element(c0, k0, m0))
        self.element_names.append(name)
        self.elements[-1].muk = self.V0 * (1 - self.elements[-1].k0) / self.elements[-1].m0

    def vparams(self, V):
        temp = self.a0 * V / self.Dl
        for el in self.elements:
            el.kv = (el.k0 + temp) / (1 + temp)
            el.mv = el.m0 * (1 + ((el.k0 - el.kv + el.kv * np.log(el.kv / el.k0)) / (1 - el.k0)))

    def calc_Tliq(self):
        self.Tliq = self.Tf
        for el in self.elements:
            self.Tliq += el.m0 * el.c0

    def dendriteTip(self, G, V):
        R = 1e-8
        not_conv = True
        tol = 1e-5
        relax = 0.9

        self.vparams(V)

        # Converge tip radius
        while(not_conv):
            Pe = R * V / (2 * self.Dl)

            denom = 0
            for el in self.elements:
                Ct = el.c0 / (1 - (1 - el.kv)*Iv3d(Pe))
                el.xic = np.sqrt(1 + (1 / (self.sigst * Pe**2))) - 1 + 2*el.kv
                el.xic = 1 - (2 * el.kv / el.xic)
                Gc = -(V / self.Dl) * Ct * (1 - el.kv)
                denom += (-el.mv * Gc * el.xic)

            denom = self.sigst * (-denom - G)
            R_temp = R
            R = np.sqrt(self.gam / denom)
            R = R_temp + relax * (R - R_temp)
            dif = np.abs(R - R_temp)
            if dif/R < tol or np.isnan(R):
                not_conv = False

        # Loop through alloying elements
        constitutional = 0
        kinetics = 0
        for el in self.elements:
            # Constitutional supercooling
            dTv = el.mv * el.c0 * (el.kv - 1) / el.kv
            constitutional += (el.kv * dTv * Iv3d(Pe)) / (1 - (1 - el.kv) * Iv3d(Pe))
            constitutional = constitutional + (el.m0 - el.mv) * el.c0

            # Kinetic undercooling
            kinetics += V / el.muk

        # Thermal undercooling
        Peclet_thermal = V * R / (2 * self.alpha)
        thermal = self.Lf * Iv3d(Peclet_thermal) / self.cl

        # Curvature undercooling
        curvature = (2 * self.gam / R)

        # Cellular undercooling
        cell = G * self.Dl / V

        # Total undercooling
        total = constitutional + curvature + kinetics + cell + thermal

        return R, total, constitutional, curvature, kinetics, cell, thermal

    def Gibbs_Thomson_Ceff(self, Interfacial_Energy, Enthalpy_of_Fusion_PerVol, solidusTemperature):
        
        Gibbs_Thomson_Coeff = (Interfacial_Energy*solidusTemperature)/(Enthalpy_of_Fusion_PerVol)
        print('Gibbs', Gibbs_Thomson_Coeff)
        return Gibbs_Thomson_Coeff
    
    def Calcualte_Interfacial_Energy(self, enthalpy_of_fusion_Molar, MolarVolume, alpha = 0.44):
        
        ## TURNBULLs APPROXIMATION FOR Interfacial Energy
        
        Avagadros_Number = 6.0221409e+23
        
        Interfacial_Energy = alpha * enthalpy_of_fusion_Molar/(MolarVolume**(2/3)*Avagadros_Number**(1/3))
        
        return Interfacial_Energy
    
    def generate_coefficients_Thermocalc(self, start, composition, main_phase, database, balance, start_temperature = 2000, extra_phases=None):
        
        
        ## Define Overall System
        system = start.select_database_and_elements(database, composition.keys()).without_default_phases().select_phase("liquid")
        system.select_phase(main_phase)
        if extra_phases != None:     
            for ph in extra_phases:
                system.select_phase(ph)
        
        
        
        PhysicalProperties = system.get_system()
        calculation = PhysicalProperties.with_single_equilibrium_calculation()
        calculation.set_condition("P", 100000.0)
        calculation.set_condition("T", start_temperature)
        calculation.set_condition("N", 1.0)
        
        for keys, values in list(composition.items()):
            if values > -1:
                calculation.set_condition("w(" + keys + ")", values * 10 ** -2)

        calculation.calculate()

        calculation.run_poly_command("s-c t=NONE")
        calculation.set_phase_to_fixed(main_phase,0)
        results = calculation.calculate()
        
        
        ## Liquidus Slope and Liquidus Temperature Calcualtion
        liquidus_slope = {}
        Phase_composition = {}
        liquid_composition = {}
        for keys in composition.keys():
            if keys != balance:
                liquidus_slope[keys] = results.get_value_of("T.W(" + keys + ")")
            Phase_composition[keys] = results.get_value_of("w(" + main_phase + "," + keys + ")")
            liquid_composition[keys] = results.get_value_of("w(LIQUID," + keys + ")")
        liquidus_temperature = results.get_value_of("T")
        partition_coefficients = {}

        for keys in composition.keys():
            partition_coefficients[keys] = Phase_composition[keys] / liquid_composition[keys]
        self.Tliq = liquidus_temperature
        self.Tf = liquidus_temperature
        
        
        ## Density Calculation
        DensityTemperature = 23
        calculation.run_poly_command("s-c t=" + str(DensityTemperature+273))
        calculation.set_phase_to_entered(main_phase, 0)
        results = calculation.calculate()
        MolarVolume = results.get_value_of('vm')
        B = results.get_value_of('b')
        Density = (B*1e-3)/MolarVolume ## Fixed
        self.rho = Density ## Density:: Figure out how to get this from TCAL
        
        
        
        ## Solidus Temperature Calcualtion
        try:
            calculation.run_poly_command("s-c t=NONE")
            calculation.set_phase_to_fixed("liquid", 0)
            results = calculation.calculate()
            solidusTemperature = results.get_value_of("T")
        except:
            solidusTemperature = None

        
        
        


        ## Specific Heat Calcualtion
        calculation = PhysicalProperties.with_property_diagram_calculation()
        for keys, values in list(composition.items()):
            if values > -1:
                calculation.set_condition("w(" + keys + ")", values * 10 ** -2)
        results = calculation.with_axis(CalculationAxis(ThermodynamicQuantity.temperature()).set_min(23+273).set_max(24+273)).\
            calculate().get_values_grouped_by_quantity_of('T', 'CP=HM.T') 
        self.cl = results[''].y[0]/(B*1e-3) ## only at room temperature (need to find a way to specify)



        ## Schiel Calcaultor for latent heat of fusion.
        Schiel_Calculator = system.get_system_for_scheil_calculations().with_scheil_calculation().set_composition_unit(CompositionUnit.MASS_PERCENT)                                                                                                                                          
        
        for keys, values in list(composition.items()):
            if values > -1:
                Schiel_Calculator.set_composition(keys, values)
    
        result = Schiel_Calculator.calculate()
        t_latent_heat, latent_heat = result.get_values_of(ScheilQuantity.temperature(), ScheilQuantity.latent_heat_per_mole())
        enthalpy_of_fusion_Molar = np.max(np.abs(latent_heat))  

        self.Lf =  enthalpy_of_fusion_Molar ##Latent Heat of Fusion:: probably not right but working on it not sure on units is it Kj/Kg, j/kg, j/g..
        


        ## Determination of Additional Properties
        Interfacial_Energy = self.Calcualte_Interfacial_Energy(enthalpy_of_fusion_Molar, MolarVolume, alpha = 0.44)
        self.gam = self.Gibbs_Thomson_Ceff(Interfacial_Energy, enthalpy_of_fusion_Molar/MolarVolume, liquidus_temperature)
        self.alpha = self.ThermalConductivity/(self.rho*self.cl)
    
    
    
        
        ## Defining Elements for the system
        for keys in dict(composition.items()).keys():
            if keys == balance:
                continue            
            self.add_element(name = keys, c0 = composition[keys]/100, k0 = partition_coefficients[keys], m0 = liquidus_slope[keys])
        
        
        return liquidus_temperature, solidusTemperature, self.elements


    
        
# Material properties definition for KGT model
# Dl = Mass diffusivity in liquid, m2/s ;  # V0 = Speed of sound, m/s ; # a0=  Length scale for solute trapping, m


