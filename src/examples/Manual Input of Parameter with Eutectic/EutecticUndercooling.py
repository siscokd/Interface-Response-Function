# -*- coding: utf-8 -*-
"""
Created on Tue Jul 27 10:40:34 2021

@author: Kevin
"""


import numpy as np
import matplotlib.pyplot as plt
from scipy import special


class Two_Phase_Eutectic:
    def __init__(self, **kwargs):
        self.Gibbs_Alpha = kwargs.get('Gibbs_Alpha')
        self.Gibbs_Beta = kwargs.get('Gibbs_Beta')
        self.PhaseRatio = kwargs.get('PhaseRatio')
        self.Alpha_Contact_Angle = kwargs.get('AlphaContactAngle')
        self.Beta_Contact_Angle = kwargs.get('BetaContactAngle')
        self.Ra = kwargs.get('Ra')
        self.Rb = kwargs.get('Rb')
        self.elements = []
        
        
        self.Concentration = 1
        
        
    class Element:
        def __init__(self,Name,C_A,C_B,Liquidus_Slope_Alpha,Liquidus_Slope_Beta,D):
            self.C_Alpha = C_A # Concentration of At solubility limit of Element in Alpha
            self.C_Beta = C_B # Concentration of At solubility limit of element in Beta
            self.Liquidus_Slope_Alpha = Liquidus_Slope_Alpha
            self.Liquidus_Slope_Beta = Liquidus_Slope_Beta
            self.Liquid_Diffusion = D # Liquid Diffusion
        
        
    def Add_Element(self, **kwargs):
        Name = kwargs.get('name')
        C_Alpha = kwargs.get('C_Alpha')
        C_Beta = kwargs.get('C_Beta')
        Liquidus_Slope_Beta = kwargs.get('Liquidus_Slope_Beta')
        Liquidus_Slope_Alpha = kwargs.get('Liquidus_Slope_Alpha')        
        D = kwargs.get('Liquid_Diffusion')
        
        self.elements.append(self.Element(Name,C_Alpha,C_Beta,Liquidus_Slope_Alpha,Liquidus_Slope_Beta,D))
        
    def M_Calculate(self):

        kth_zero = special.jn_zeros(1,1)[-1]
        j1 = special.jn(1,((self.Ra*kth_zero)/(self.Ra+self.Rb)))
        j2 = special.jn(0,kth_zero)
        
        M = (1/kth_zero**3)*j1**2/j2**2

        return M
    
    def R_Min_Calculate(self, V):
        
        
        
        M = self.M_Calculate()


        Num = -self.Gibbs_Alpha*np.sqrt(1+self.PhaseRatio)*np.sin(self.Alpha_Contact_Angle) + self.Gibbs_Beta*(np.sqrt(1+self.PhaseRatio)/self.PhaseRatio)*np.sin(self.Beta_Contact_Angle)
        K1 = ((self.elements[0].C_Beta-self.elements[0].C_Alpha)/self.elements[0].Liquid_Diffusion)*(self.elements[0].Liquidus_Slope_Alpha + (self.elements[0].Liquidus_Slope_Beta/self.PhaseRatio))
        K2 = ((self.elements[1].C_Beta-self.elements[1].C_Alpha)/self.elements[1].Liquid_Diffusion)*(self.elements[1].Liquidus_Slope_Alpha + (self.elements[1].Liquidus_Slope_Beta/self.PhaseRatio))
        Dem = 2*V*M*(K1+K2)
        
        R_Min = np.sqrt(Num/Dem)
        
 
        return R_Min, M

        
    def Calculate_Eutectic_Temp(self, V):    
        
        R_Min, M = self.R_Min_Calculate(V)

        print(R_Min)
        K1 = ((4*self.elements[0].Liquidus_Slope_Alpha)/self.elements[0].Liquid_Diffusion)*(self.elements[0].C_Beta-self.elements[0].C_Alpha)
        K2 = ((4*self.elements[1].Liquidus_Slope_Alpha)/self.elements[1].Liquid_Diffusion)*(self.elements[1].C_Beta-self.elements[1].C_Alpha)
        Term2 = (2*self.Gibbs_Alpha*np.sqrt(1+self.PhaseRatio)*np.sin(self.Alpha_Contact_Angle))/R_Min  
        
        
        print('K1:', K1)
        print('K2:', K2)
        print('Term2:', Term2)
        Undercooling = -(K1+K2)*R_Min*M + Term2
        
        return R_Min, Undercooling



    

Gibbs_A = 1e-7
Gibbs_B = 1.3122631727347385e-07



AlCeMn = Two_Phase_Eutectic(Gibbs_Alpha=Gibbs_A,
                            Gibbs_Beta = Gibbs_B,
                            PhaseRatio=.5,
                            AlphaContactAngle=np.deg2rad(10),
                            BetaContactAngle=np.deg2rad(15),
                            Ra = 5e-9,
                            Rb = 5e-9)



AlCeMn.Add_Element(Name = 'Ce',C_Alpha = 1e-5 #The Solubility Limit of Ce in Aluminum
                              ,C_Beta = .05,     #The Solubility Limit of Ce in Al20Mn2Ce
                              Liquidus_Slope_Alpha= -(660-640)/0.1, # K/At%
                              Liquidus_Slope_Beta = -(1/0.000775),  # K/At%
                              Liquid_Diffusion=5e-9)




AlCeMn.Add_Element(Name = 'Mn', C_Alpha = .06 # The Solubility limit of Mn in Aluminum
                               ,C_Beta = .1  # The Solubility Limit of Mn in Al20Mn2Ce
                               ,Liquidus_Slope_Alpha = -(660-640)/0.1 # K/At%
                               ,Liquidus_Slope_Beta  = -(1/0.000592) # K/At%
                               ,Liquid_Diffusion=5e-9)



Radius =[]; Undercooling = []

V = np.logspace(-1,1,100)
for R,v in enumerate(V):

    r,u = AlCeMn.Calculate_Eutectic_Temp(v)
    
    Radius.append(r)
    Undercooling.append(u)
    
    
plt.plot(V,1177 -np.array(Undercooling))
plt.xscale('log')
